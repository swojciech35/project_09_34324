package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.Random;
import java.util.Scanner;

/**
 * Class responsible for ships position
 */
public class Ships {
    int[][] board=new int[10][10];

    /**
     * Constructor for create board
     */
    public Ships() {
        boardGenerate();
    }
    /**
     * Private method of inserting zeros into a two-dimensional 10x10 array
     */
    private void boardGenerate(){
        for(int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                board[i][j]=0;
            }
        }
    }
    /**
     *Public method of putting ship on the board
     * @param coordX first coordinate of the ship (A->1; B->2; C-3; D->4; E->5; F->6; G->7; H->8; I->9; J->10)
     * @param coordY second coordinate of the ship (1 to 10)
     * @param rotate direction of placing the ship on the board (right-> '>'; left-> '<'; up-> '^'; down-> 'v')
     * @param type size of the ship (1 to 4)
     * @return if ship was added-> return true | if ship wasn't added -> false
     */
    public boolean addShip(int coordX,int coordY,String rotate,int type){
        switch(rotate){
            case ">":
                if(ifPossibleToPutShips(coordX, coordY, rotate, type)==true){
                    if(coordX+type-1<= 10) {
                        for(int i=coordX-1;i<coordX+type-1;i++){
                            board[coordY-1][i]=type;
                        }
                        return true;
                    }
                }
                break;
            case "<":
                if(ifPossibleToPutShips(coordX, coordY, rotate, type)==true){
                    if(coordX-(type-1)>=0){
                        for(int i=coordX;i>=coordX-(type-1);i--){
                            board[coordY-1][i-1]=type;
                        }
                        return true;
                    }
                }
                break;
            case "^":
                if(ifPossibleToPutShips(coordX, coordY, rotate, type)==true){
                    if(coordY-(type-1)>=0){
                        for(int i=coordY;i>=coordY-(type-1);i--){
                            board[i-1][coordX-1]=type;
                        }
                        return true;
                    }
                }
                break;
            case "v":
                if(ifPossibleToPutShips(coordX, coordY, rotate, type)==true){
                    if(coordY+type-1<=10){
                        for (int i=coordY-1;i<coordY+type-1;i++){
                            board[i][coordX-1]=type;
                        }
                        return true;
                    }
                }
                break;
        }
        return false;
    }
    /**
     * Private method to check if it is possible to put the ship.
     *
     * The method checks if the ships do not overlap or touch
     *
     * @param coordX first coordinate of the ship (A->1; B->2; C-3; D->4; E->5; F->6; G->7; H->8; I->9; J->10)
     * @param coordY second coordinate of the ship (1 to 10)
     * @param rotate direction of placing the ship on the board (right-> '>'; left-> '<'; up-> '^'; down-> 'v')
     * @param type size of the ship (1 to 4)
     * @return if ship don't overlap or touch -> true | if ship do overlap or touch -> false
     */
    private boolean ifPossibleToPutShips(int coordX,int coordY,String rotate,int type){
        switch(rotate){
            case">":
                if(coordX!=1&&coordX+type<=10&&coordY!=1&&coordY!=10){
                    for(int i=coordY-1;i<coordY+2;i++){
                        for(int j=coordX-1;j<coordX+type+1;j++){
                            if(board[i-1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX==1){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j<coordX+type;j++){
                            if(board[i][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==1&&coordY==10){
                    for(int i=coordY-1;i>coordY-3;i--){
                        for(int j=coordX-1;j<coordX+type;j++){
                            if(board[i][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==1&&coordY!=1&&coordY!=10){
                    for(int i=coordY-1;i<coordY+2;i++){
                        for(int j=coordX-1;j<coordX+type;j++){
                            if(board[i-1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX+type-1<10){
                    for(int i =coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX+type-1==10){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j<coordX+type;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==10&&coordX+type-1<10){
                    for (int i=coordY-1;i>coordY-3;i--){
                        for(int j=coordX-1;j<coordX+type+1;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==10&&coordX+type-1==10){
                    for (int i=coordY-1;i>coordY-3;i--){
                        for(int j=coordX-1;j<coordX+type;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX+type-1==10&&coordY!=1&&coordY!=10){
                    for(int i=coordY-1;i<coordY+2;i++){
                        for(int j=coordX-1;j<coordX+type;j++){
                            if(board[i-1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }
                break;
            case"<":
                if(coordX!=10&&coordX-type>=1&&coordY!=1&&coordY!=10){
                    for(int i=coordY-1;i<coordY+2;i++){
                        for(int j=coordX-1;j>coordX-(type+3);j--){
                            if(board[i-1][j+1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==10&&coordY==1){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j>coordX-(type+2);j--){
                            if(board[i][j]!=0){
                                return false;
                            }
                        }
                    }
                }
                else if(coordX==10&&coordY==10){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j>coordX-(type+2);j--){
                            if(board[i-1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==10&&coordY!=1&&coordY!=10){
                    for(int i=coordY-1;i<coordY+2;i++){
                        for(int j=coordX-1;j>coordX-(type+2);j--){
                            if(board[i-1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX-type+1>1){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX+1;j>coordX-(type+1);j--){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX-type+1==1){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX+1;j>coordX-type;j--){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==10&&coordX!=10&&coordX-type+1>1){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j>coordX-(type+3);j--){
                            if(board[i-1][j+1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==10&&coordX-type+1==1){
                    for(int i=coordY-1;i<coordY+1;i++){
                        for(int j=coordX-1;j>coordX-(type+2);j--){
                            if(board[i-1][j+1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY!=1&&coordY!=10&&coordX-type+1==1){
                    for(int i=coordY-1;i<coordY+2;i++){
                        for(int j=coordX-1;j>coordX-(type+2);j--){
                            if(board[i-1][j+1]!=0){
                                return false;
                            }
                        }
                    }
                }
                break;
            case"^":
                if(coordY!=10&&coordY-type>=1&&coordX!=1&&coordX!=10){
                    for(int i=coordY-1;i>coordY-(type+3);i--){
                        for(int j=coordX-1;j<coordX+2;j++){
                            if(board[i+1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==1&&coordY==10){
                    for(int i=coordY-1;i>coordY-(type+2);i--){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==10&&coordY==10){
                    for(int i=coordY-1;i>coordY-(type+2);i--){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==10&&coordX!=1&&coordX!=10){
                    for(int i=coordY-1;i>coordY-(type+2);i--){
                        for(int j=coordX-1;j<coordX+2;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY-type==0&&coordX==1){
                    for(int i=coordY-1;i>coordY-(type+2);i--){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i+1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY-type==0&&coordX==10){
                    for(int i=coordY-1;i>coordY-(type+2);i--){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i+1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY-type==0&&coordX!=1&&coordX!=10){
                    for(int i=coordY-1;i>coordY-(type+2);i--){
                        for(int j=coordX-1;j<coordX+2;j++){
                            if(board[i+1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==1&&coordY!=10&&coordY-type!=0) {
                    for(int i=coordY-1;i>coordY-(type+3);i--){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i+1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==10&&coordY!=10&&coordY-type!=0){
                    for(int i=coordY-1;i>coordY-(type+3);i--){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i+1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }
                break;
            case"v":
                if(coordY!=1&&coordY+type<=10&&coordX!=10&&coordX!=1){
                    for(int i=coordY-1;i<coordY+type+1;i++){
                        for(int j=coordX-1;j<coordX+2;j++){
                            if(board[i-1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX==1){
                    for(int i=coordY-1;i<coordY+type;i++){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX==10){
                    for(int i=coordY-1;i<coordY+type;i++){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==1&&coordY+type-1==10){
                    for(int i=coordY-1;i<coordY+type;i++){
                        for(int j=coordX-1;i<coordX+1;j++){
                            if(board[i-1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordX==10&&coordY+type-1==10){
                    for(int i=coordY-1;i<coordY+type;i++){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i-1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY==1&&coordX!=1&&coordX!=10){
                    for(int i=coordY-1;i<coordY+type;i++){
                        for(int j=coordX-1;j<coordX+2;j++){
                            if(board[i][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY+type-1==10&&coordX!=1&&coordY!=10){
                    for(int i=coordY-1;i<coordY+type;i++){
                        for(int j=coordX-1;j<coordX+2;j++){
                            if(board[i-1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY!=1&&coordY+type-1<10&&coordX==1){
                    for(int i=coordY-1;i<coordY+type+1;i++){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i-1][j]!=0){
                                return false;
                            }
                        }
                    }
                }else if(coordY!=1&&coordY+type-1<10&&coordX==10){
                    for(int i=coordY-1;i<coordY+type+1;i++){
                        for(int j=coordX-1;j<coordX+1;j++){
                            if(board[i-1][j-1]!=0){
                                return false;
                            }
                        }
                    }
                }
                break;
        }
        return true;
    }
    /**
     * Public method to select a random configuration file
     * @throws FileNotFoundException
     */
    public void loadShips() throws FileNotFoundException {
        Random rand =new Random();
        String config="config"+rand.nextInt(5)+".txt";
        readFromFile(config);
    }
    /**
     * Private method to read file and load enemy board from file
     * @param fileName confifguration file name
     * @throws FileNotFoundException
     */
    private void readFromFile(String fileName) throws FileNotFoundException {
        File file=new File(fileName);
        Scanner configFile=new Scanner(file);
        for(int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                board[i][j]=configFile.nextInt();
            }
        }
        configFile.close();
    }
}
