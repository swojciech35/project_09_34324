package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class responsible for the window with setting the ships
 */
public class Menu implements ActionListener {
    JFrame menuWindow = new JFrame("Menu");
    JButton[] myBoard = new JButton[100];
    JPanel fullPanel = new JPanel();
    Ships myBoardInfo = new Ships();
    JPanel shipsButtonsPanel = new JPanel(new FlowLayout());
    JButton[] shipsButtons = new JButton[4];
    JPanel nextButtonPanel = new JPanel(new GridLayout());
    JButton nextButton = new JButton();
    int[] ships = new int[4];
    int currentType = 0;
    int currentCoords = 0;

    /**
     * Constructor for class Menu
     */
    Menu() {
        fillShips();
        createFrame(menuWindow, 600, 800);
        createShipsButtonsPanel();
        setNextButton();
        createPanel(fullPanel);
        fullPanel.add(createTitle("SET YOUR SHIPS"));
        fullPanel.add(createBoard(myBoard, true, myBoardInfo.board, this, true));
        fullPanel.add(shipsButtonsPanel);
        fullPanel.add(nextButtonPanel);
        menuWindow.add(fullPanel);
        menuWindow.setVisible(true);

    }

    /**
     * Public static method of setting JPanel to specific layout, size, color and border
     *
     * @param panel JPanel to customize
     */
    public static void createPanel(JPanel panel) {
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBounds(0, 0, 600, 800);
        panel.setBackground(null);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    }

    /**
     * Public static method of setting JFrame to specific properties and given size
     *
     * @param frame  JFrame to customize
     * @param width  width of JFrame to be set
     * @param height height of JFrame to be set
     */
    public static void createFrame(JFrame frame, int width, int height) {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(width, height);
        frame.setLayout(null);
        frame.getContentPane().setBackground(new Color(195, 236, 244));
        frame.setResizable(false);
    }

    /**
     * Public static method that creates JPanel - a grid 10x10 of buttons that imitates a board. The buttons have either coords (empty field) or X (ship) as text.
     * Depending on the given parameters, buttons can be clickable or not clickable.
     *
     * @param field array of buttons that will be placed in the grid
     * @param XVisible true -> show ships on board as "X" on button | false -> don't show ships
     * @param boardInfo two-dimensional array with information where are the ships (if board[index] is 0 -> there is no ship | if board[index] is from 1 to 4 -> there is a part of ship with a size of value at board[index])
     * @param listener ActionListener
     * @param ifClick true -> buttons on board are clickable | false -> buttons on board aren't clickable
     * @return JPanel with grid of buttons
     */
    public static JPanel createBoard(JButton[] field, boolean XVisible, int[][] boardInfo, ActionListener listener, boolean ifClick) {
        JPanel tmpPanel = new JPanel(new GridLayout(10, 10));
        tmpPanel.setMaximumSize(new Dimension(500, 500));
        tmpPanel.setBackground(null);
        tmpPanel.setAlignmentX(SwingConstants.CENTER);
        for (int i = 0; i < 100; i++) {
            field[i] = new JButton();
            if (ifClick == true) {
                field[i].addActionListener(listener);
            }
            field[i].setFocusable(false);
            field[i].setBackground(new Color(155, 219, 233));
            field[i].setText("<html><font color=rgba(0,0,0,0.3)>" + NumbersToCoords(i) + "</font></html>");
            if (XVisible == true && boardInfo[numberDiv(i)][numberMod(i)] > 0) {
                field[i].setText("X");
            }
            tmpPanel.add(field[i]);
        }
        tmpPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        return tmpPanel;
    }

    /**
     * Method to refresh the board given the array with information where the ships are placed; used after lacing the new ship
     * @param field array of buttons (board)
     * @param boardInfo two-dimensional array with information about ships placement
     */
    void refreshGrid(JButton[] field, int[][] boardInfo) {
        for (int i = 0; i < 100; i++) {
            if (boardInfo[numberDiv(i)][numberMod(i)] > 0) {
                field[i].setText("X");
                field[i].setBackground(new Color(82, 190, 210));
            } else {
                field[i].setText("<html><font color=rgba(0,0,0,0.3)>" + NumbersToCoords(i) + "</font></html>");
                field[i].setBackground(new Color(155, 219, 233));
            }
        }
    }

    /**
     * Public static method that creates, customizes and returns JPanel containing JLabel with given title
     * @param text title to be set in JLabel
     * @return created JPanel
     */
    public static JPanel createTitle(String text) {
        JPanel tmpPanel = new JPanel();
        JLabel title = new JLabel("<html><font size = 8><center>" + text + "</center></font></html>");
        tmpPanel.setBackground(null);
        tmpPanel.setMaximumSize(new Dimension(500, 60));
        tmpPanel.add(title);
        return tmpPanel;
    }

    /**
     * Private method that customizes JPanel with buttons used to pick size of the ship
     */
    void createShipsButtonsPanel() {
        JLabel text = new JLabel("Pick size of a ship:");
        shipsButtonsPanel.add(text);
        for (int i = 0; i < 4; i++) {
            shipsButtons[i] = new JButton();
            shipsButtons[i].setText(String.valueOf(i + 1));
            shipsButtons[i].setBackground(new Color(155, 219, 233));
            shipsButtons[i].setFocusable(false);
            shipsButtons[i].addActionListener(this);
            shipsButtonsPanel.add(shipsButtons[i]);
        }
        shipsButtonsPanel.setMaximumSize(new Dimension(500, 90));
        shipsButtonsPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        shipsButtonsPanel.setBackground(null);
    }

    /**
     * Private method that customizes JPanel with button "Next"
     */
    void setNextButton() {
        nextButton.setBackground(new Color(155, 219, 233));
        nextButton.setText("NEXT");
        nextButton.setFocusable(false);
        nextButton.setEnabled(false);
        nextButton.addActionListener(this);
        nextButtonPanel.setBackground(null);
        nextButtonPanel.setMaximumSize(new Dimension(500, 50));
        nextButtonPanel.add(nextButton);
    }

    /**
     * Public method that returns coordinates on 10x10 board from the given number: 0 -> A1, 1 -> A2, [...] 99 ->  J10
     * @param number number to convert
     * @return coordinates from number as text
     */
    public static String NumbersToCoords(int number) {
        int column_int;
        String column = "";
        String row = String.valueOf(number / 10 + 1);
        column_int = number % 10;
        switch (column_int) {
            case 0:
                column = "A";
                break;
            case 1:
                column = "B";
                break;
            case 2:
                column = "C";
                break;
            case 3:
                column = "D";
                break;
            case 4:
                column = "E";
                break;
            case 5:
                column = "F";
                break;
            case 6:
                column = "G";
                break;
            case 7:
                column = "H";
                break;
            case 8:
                column = "I";
                break;
            case 9:
                column = "J";
                break;
        }
        return column + row;
    }

    /**
     * Public static method returning number divided by 10
     * @param number number to be divided
     * @return number divided by 10
     */
    public static int numberDiv(int number) {
        return (number / 10) % 10;
    }

    /**
     * Public static method returning remainder from number divided by 10
     * @param number number to be divided
     * @return remainder from number divided by 10
     */
    public static int numberMod(int number) {
        return (number % 10) % 10;
    }

    /**
     * Method that changes text of buttons around the given button on board (if possible): button on the right to '>', button on the left to '<', button below to 'v', button above to '^'
     *
     * @param tab array of buttons (the board on which arrows will be dispalyed)
     * @param coord index of button around which arrows are to be set
     */
    void arrows(JButton[] tab, int coord) {
        tab[coord].setBackground(new Color(82, 190, 210));
        tab[coord].setText("X");
        if (numberMod(coord) > 0) {
            tab[coord - 1].setText("<");
            tab[coord - 1].setBackground(new Color(122, 210, 229));
        }
        if (numberMod(coord) < 9) {
            tab[coord + 1].setText(">");
            tab[coord + 1].setBackground(new Color(122, 210, 229));
        }
        if (numberDiv(coord) > 0) {
            tab[coord - 10].setText("^");
            tab[coord - 10].setBackground(new Color(122, 210, 229));
        }
        if (numberDiv(coord) < 9) {
            tab[coord + 10].setText("v");
            tab[coord + 10].setBackground(new Color(122, 210, 229));
        }

    }

    /**
     * Private method that fills 'ships' array (ships[size of the ship - 1] = number of ships with that size to be set on board)
     */
    private void fillShips() {
        ships[0] = 4;
        ships[1] = 3;
        ships[2] = 2;
        ships[3] = 1;
    }

    /**
     * Method that colours buttons used to pick size of the ship to specific color and highlights size given in parameter
     *
     * @param type size of the ship to be highlighted; no size to be highlighted if type is 0
     */
    void colorShipButtons(int type) {
        int control = 0;

        for (int i = 0; i < 4; i++) {

            if (type != 0) {
                if (ships[i] > 0) {
                    control += ships[i];
                    if (i != type - 1) {
                        shipsButtons[i].setBackground(new Color(155, 219, 233));
                    } else {
                        shipsButtons[i].setBackground(new Color(122, 210, 229));
                    }
                } else {
                    shipsButtons[i].setBackground(new Color(239, 65, 65));
                    shipsButtons[i].setEnabled(false);
                }

            } else {
                if (ships[i] > 0) {
                    control += ships[i];
                    shipsButtons[i].setBackground(new Color(155, 219, 233));
                } else {
                    shipsButtons[i].setBackground(new Color(239, 65, 65));
                    shipsButtons[i].setEnabled(false);
                }
            }

        }

        if (control == 0) {
            nextButton.setEnabled(true);
        }

    }

    /**
     * Method invoked when an action occurs
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < 100; i++) {
            if (e.getSource() == myBoard[i]) {
                if (currentType != 0) {
                    if (myBoard[i].getText().equals(">") || myBoard[i].getText().equals("<") || myBoard[i].getText().equals("^") || myBoard[i].getText().equals("v")) {
                        if (myBoardInfo.addShip(numberMod(currentCoords) + 1, numberDiv(currentCoords) + 1, myBoard[i].getText(), currentType)) {
                            ships[currentType - 1]--;
                        } else {
                            showError("Couldn't place the ship, try again with different size or direction");
                        }
                        refreshGrid(myBoard, myBoardInfo.board);
                        currentType = 0;
                        colorShipButtons(currentType);
                    } else {
                        currentCoords = i;
                        refreshGrid(myBoard, myBoardInfo.board);
                        arrows(myBoard, i);
                    }
                } else {
                    showError("Pick size of a ship");
                }

            }
        }

        for (int i = 0; i < 4; i++) {
            if (e.getSource() == shipsButtons[i]) {
                currentType = i + 1;
                colorShipButtons(currentType);
            }
        }

        if (e.getSource() == nextButton) {
            menuWindow.dispose();
            Game game = new Game(myBoardInfo.board);
        }
    }

    /**
     * Method that shows a small error window with given message
     *
     * @param message message of error to be dispalyed
     */
    void showError(String message) {
        JOptionPane.showMessageDialog(null, message, "Setting ships error", JOptionPane.ERROR_MESSAGE);
    }


}
