package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Random;

import static com.company.Menu.*;

/**
 * Class responsible for the window with the game
 */
public class Game implements ActionListener {
    int[][] myBoardInfo;
    int[][] enemyBoardInfo;
    int[] myShips = new int[4];
    int[] enemyShips = new int[4];

    JFrame gameWindow = new JFrame("Game");
    JPanel myPanel = new JPanel();
    JPanel enemyPanel = new JPanel();
    JButton[] myBoard = new JButton[100];
    JButton[] enemyBoard = new JButton[100];
    JPanel myShipsPanel = new JPanel();
    JLabel myShipsInfo = new JLabel();
    JPanel enemyShipsPanel = new JPanel();
    JLabel enemyShipsInfo = new JLabel();

    /**
     * Constructor for class Game
     * @param myBoardImported two-dimensional array with information about player's ships
     */
    Game(int[][] myBoardImported) {
        myBoardInfo = myBoardImported;
        enemyBoardInfo = randomBoard();
        createFrame(gameWindow, 1200, 800);
        createPanel(myPanel);
        createPanel(enemyPanel);
        createShipPanels();
        myPanel.add(createTitle("MY SHIPS"));
        enemyPanel.add(createTitle("ENEMY SHIPS"));
        myPanel.add(createBoard(myBoard, true, myBoardInfo, this, false));
        enemyPanel.add(createBoard(enemyBoard, false, enemyBoardInfo, this, true));
        myPanel.add(myShipsPanel);
        enemyPanel.add(enemyShipsPanel);
        gameWindow.setLayout(new GridLayout(1, 2));
        gameWindow.add(myPanel);
        gameWindow.add(enemyPanel);
        gameWindow.setVisible(true);
    }

    /**
     * Method that sets JPanels with information about sunk ships to specific size, color and adds respective JLabels
     */
    void createShipPanels() {
        for (int i = 0; i < 4; i++) {
            enemyShips[i] = 0;
            myShips[i] = 0;
        }
        myShipsPanel.setMaximumSize(new Dimension(500, 200));
        enemyShipsPanel.setMaximumSize(new Dimension(500, 200));
        myShipsPanel.setBackground(null);
        enemyShipsPanel.setBackground(null);
        myShipsPanel.add(myShipsInfo);
        enemyShipsPanel.add(enemyShipsInfo);
        refreshShipInfo();
    }

    /**
     * Private method that fills JLabels with information about how many ships are sunk already
     */
    private void refreshShipInfo() {
        enemyShipsInfo.setText("<html><font size = 5><center><br><u>Sunk ships:</u><br/>4 - " + enemyShips[3] + "/1<br/>3 - " + enemyShips[2] + "/2<br/>2 - " + enemyShips[1] + "/3<br/>1 - " + enemyShips[0] + "/4</center></font></html>");
        myShipsInfo.setText("<html><font size = 5><center><br><u>Sunk ships:</u><br/>4 - " + myShips[3] + "/1<br/>3 - " + myShips[2] + "/2<br/>2 - " + myShips[1] + "/3<br/>1 - " + myShips[0] + "/4</center></font></html>");

    }

    /**
     * Method that creates and returns two-dimensional array with information about ships placement, generated randomly from configuration files
     * @return 10x10 array with information about ships palcement
     */
    int[][] randomBoard() {
        Ships enemyBoardRand = new Ships();
        try {
            enemyBoardRand.loadShips();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return enemyBoardRand.board;
    }

    /**
     * Method that "hits" specific button by changing its appearance on board and refreshes the infomation about sunk ships if any was hit
     * @param board array of buttons (board)
     * @param boardInfo information about ships placement
     * @param coord number of button to be hit
     * @param ships array with information about sunk ships
     */
    void hit(JButton[] board, int[][] boardInfo, int coord, int[] ships) {

        if (boardInfo[numberDiv(coord)][numberMod(coord)] < 1) {
            board[coord].setText("");
            board[coord].setBackground(new Color(255, 140, 140));
        } else {
            board[coord].setText("X");
            board[coord].setBackground(new Color(239, 65, 65));
            if (!ifAround(board, coord, coord, boardInfo)) {
                ships[boardInfo[numberDiv(coord)][numberMod(coord)] - 1]++;
                refreshShipInfo();
            }
        }
    }

    /**
     * Method that checks if button was already hit
     * @param button button to check
     * @return true -> button was hit already | false -> button wasn't hit
     */
    boolean ifHit(JButton button) {
        Color tmpColor = new Color(155, 219, 233);
        return button.getBackground().getRGB() != tmpColor.getRGB();
    }

    /**
     * Recursive method that checks if there is any button with ship that wasn't hit touching given button
     * @param tab array of buttons
     * @param previousCoord number of previous button to stop method from infnite looping
     * @param coord number of button to check
     * @param boardInfo two-dimensional array with information about ships placement
     * @return true -> there are parts of the ship near the button that weren't hit yet | false -> given button is the last part of ship and the whole ship is sunk
     */
    boolean ifAround(JButton[] tab, int previousCoord, int coord, int[][] boardInfo) {
        if (numberMod(coord) > 0) {
            if (coord - 1 != previousCoord && ifHit(tab[coord - 1]) && boardInfo[numberDiv(coord - 1)][numberMod(coord - 1)] > 0) {
                if (ifAround(tab, coord, coord - 1, boardInfo)) return true;
            }
            if (!ifHit(tab[coord - 1]) && boardInfo[numberDiv(coord - 1)][numberMod(coord - 1)] > 0) {
                return true;
            }
        }
        if (numberMod(coord) < 9) {
            if (coord + 1 != previousCoord && ifHit(tab[coord + 1]) && boardInfo[numberDiv(coord + 1)][numberMod(coord + 1)] > 0) {
                if (ifAround(tab, coord, coord + 1, boardInfo)) return true;
            }
            if (!ifHit(tab[coord + 1]) && boardInfo[numberDiv(coord + 1)][numberMod(coord + 1)] > 0) {
                return true;
            }
        }

        if (numberDiv(coord) > 0) {
            if (coord - 10 != previousCoord && ifHit(tab[coord - 10]) && boardInfo[numberDiv(coord - 10)][numberMod(coord - 10)] > 0) {
                if (ifAround(tab, coord, coord - 10, boardInfo)) return true;
            }
            if (!ifHit(tab[coord - 10]) && boardInfo[numberDiv(coord - 10)][numberMod(coord - 10)] > 0) {
                return true;
            }
        }
        if (numberDiv(coord) < 9) {
            if (coord + 10 != previousCoord && ifHit(tab[coord + 10]) && boardInfo[numberDiv(coord + 10)][numberMod(coord + 10)] > 0) {
                if (ifAround(tab, coord, coord + 10, boardInfo)) return true;
            }
            if (!ifHit(tab[coord + 10]) && boardInfo[numberDiv(coord + 10)][numberMod(coord + 10)] > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method invoked when an action occurs
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < 100; i++) {
            if (e.getSource() == enemyBoard[i]) {
                if (!ifHit(enemyBoard[i])) {
                    hit(enemyBoard, enemyBoardInfo, i, enemyShips);
                    if (ifEnd(enemyShips)) {
                        endOfGame(true, gameWindow);
                    }

                    int randomHit = randomNumber(100);
                    while (ifHit(myBoard[randomHit])) {
                        randomHit = randomNumber(100);
                    }
                    hit(myBoard, myBoardInfo, randomHit, myShips);
                    if (ifEnd(myShips)) {
                        endOfGame(false, gameWindow);
                    }
                }

            }
        }
    }

    /**
     * Method to display result and close the game window
     * @param ifWon true -> player won | false -> player lost
     * @param window JFrame to close
     */
    void endOfGame(boolean ifWon, JFrame window) {
        if (ifWon) {
            JOptionPane.showMessageDialog(null, "You won!", "Victory", JOptionPane.PLAIN_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "You lost!", "Defeat", JOptionPane.PLAIN_MESSAGE);
        }
        window.dispose();
    }

    /**
     * Method that generates pseudorandom number from 0 to given bound - 1
     * @param bound max number + 1
     * @return pseudorandom number
     */
    int randomNumber(int bound) {
        Random r = new Random();
        return r.nextInt(bound);
    }

    /**
     * Private method that checks if there are any ships on board left
     * @param sunkShips array with information how many ships are sunk
     * @return true -> all ships are sunk | false -> there are ships left on board
     */
    private boolean ifEnd(int[] sunkShips) {
        if (sunkShips[0] == 4 && sunkShips[1] == 3 && sunkShips[2] == 2 && sunkShips[3] == 1) {
            return true;
        }
        return false;
    }
}

