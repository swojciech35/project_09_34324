# BATTLESHIPS GAME #

The battleships game was built for school project.


### Technologies Used ###

* Java- SDK version 17 
* Abstract Window Toolkit 


### Instruction ###

1. Run project.
2. Select size of ship.
3. Click on the box in board.
4. Select an arrow and add a ship in the selected location.Ships cannot touch each other.
5. If you put all ships, click NEXT.
6. Click on enemy board to hit the enemy ship  and have fun.

### Distribution of tasks ###

| Katarzyna Bączek       | Wojciech Schabowski          |
|------------------------|------------------------------|
| Gui                    | function to put ships        |
| logic function of game | function to load enemy ships | 
| documentation          | documentation                |

### Gallery ###
![menu](https://cdn.discordapp.com/attachments/846345321244131339/956242907642359929/unknown.png)
![Game](https://cdn.discordapp.com/attachments/908302616365256745/956309674314047578/unknown.png)

### Created by: ###
* Katarzyna Bączek     [BITBUCKET](https://bitbucket.org/kasia34270)
* Wojciech Schabowski  [BITBUCKET](https://bitbucket.org/swojciech35)


